<?php /* Place custom functions here */

//include ('style.css');

function get_custom_financing_percentage(){
	return 10; // %
}

// 1 = bi-weekly
// 2 = weekly
// 3 = monthly
function get_custom_financing_frequency(){
	return 3;
}

function get_custom_balloon_percentage(){
	return 40; // %
}


function custom_financing_calculator_script() {
    wp_dequeue_script( 'automotive-listing-financing-calculator' );

    //wp_enqueue_script('automotive-listing-financing-calculator-custom', trailingslashit(get_stylesheet_directory_uri()) . "financing-calculator.js", array( 'jquery' ), AUTOMOTIVE_VERSION );
}
add_action( 'wp_enqueue_scripts', 'custom_financing_calculator_script', 15 );


add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}


function image_function() {
     return '<img src="https://mademanjournal.com/wp-content/uploads/2019/10/bfd40d70ccfeaff607ea9c0ca23264fc.jpg"
     width="540" height="280" class="left-align" />';
}
add_shortcode('InteriorImage', 'image_function');


function video_shortcode( $atts ) {
    extract( shortcode_atts( array (
        'identifier' => 'wH49n-4zuIc'
    ), $atts ) );
    return '<div class="wptuts-video-container"><iframe src="//www.youtube.com/embed/' . $identifier . '" height="240" width="320" allowfullscreen="" frameborder="0"></iframe></div>
    <!--.wptuts-video-container-->';
}
add_shortcode ('MustangVideo', 'video_shortcode' );


function recent_posts_function($atts){
   extract(shortcode_atts(array(
      'posts' => 1,
   ), $atts));

   $return_string = '<ul>';
   query_posts(array('orderby' => 'date', 'order' => 'DESC' , 'showposts' => $posts));
   if (have_posts()) :
      while (have_posts()) : the_post();
         $return_string .= '<li><a class="custompost" href="'.get_permalink().'">'.get_the_title().'</a></li>';
      endwhile;
   endif;
   $return_string .= '</ul>';

   wp_reset_query();
   return $return_string;
}
add_shortcode('recent-posts', 'recent_posts_function');


function custom_button_shortcode( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'url'    => '',
        'title'  => '',
        'target' => '',
        'text'   => '',
    ), $atts ) );

    $content = $text ? $text : $content;

    if ( $url ) {

        $link_attr = array(
            'href'   => esc_url( $url ),
            'title'  => esc_attr( $title ),
            'target' => ( 'blank' == $target ) ? '_blank' : '',
            'class'  => 'custombutton'
        );

        $link_attrs_str = '';

        foreach ( $link_attr as $key => $val ) {

            if ( $val ) {

                $link_attrs_str .= ' ' . $key . '="' . $val . '"';

            }

        }


        return '<a class="custombutton" type="button"' . $link_attrs_str . '><span class="custombutton">' . do_shortcode( $content ) . '</span></a>';

    }

    else {

        return '<span class="custombutton"><span>' . do_shortcode( $content ) . '</span></span>';

    }

}
add_shortcode( 'custombutton', 'custom_button_shortcode' );

function MaltaDate() {
	date_default_timezone_set('Europe/Rome');
	return '<span class="customdate">' . date('d/m/Y h:i a', time()) . '</span>';
}
add_shortcode( 'MaltaDate', 'MaltaDate' );


function pdf_function($attr, $url) {
   extract(shortcode_atts(array(
       'width' => '640',
       'height' => '480'
   ), $attr));
   return '<iframe src="http://docs.google.com/viewer?url=' . $url . '&embedded=true" style="width:' .$width. '; height:' .$height. ';">Your browser does not support iframes</iframe>';
}
add_shortcode('pdf', 'pdf_function');
